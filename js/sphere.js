function Sphere(position, radius, material) {
	this.position = position
	this.radius = radius
	this.material = material
}

/**
 * Check if a ray specified by a given source and direction intersects with the sphere. There are 
 * three possibilities: there is no intersection, there is a single point of intersection (ray 
 * grazes the surface), or there are two points of intersection (entry and exit).
 */
Sphere.prototype.rayIntersect = function(source, direction) {
    
    // l is the vector from the source to the center of the sphere
    let l = vectorSubtract(this.position, source)
    
    // a is the dot product of the ray direction and l and therefore is the
    // magnitude of the projection of l on the ray as the ray direction is a unit vector
    let a = vectorDotProduct(l, direction)
    
    // d is the distance between the sphere center and the ray. d2 is the square of d,
    // calculated below using Pythagoras' theorem
    let d2 = vectorDotProduct(l, l) - a * a
    
    // If d is greater than the radius of the sphere the ray does not intersect
    if (d2 > this.radius * this.radius) return { intersects: false }
    
    // b is the distance of the ray from the surface of the sphere
    let b = Math.sqrt(this.radius * this.radius - d2)
    
    // t0 is the distance from the source to the entry intersection of the ray with the sphere
    let t0 = a - b
    // t1 is the distance from the source to the exit intersection of the ray with the sphere
    let t1 = a + b
    
    // If t0 is less than 0 and t1 is greater, the source is inside the sphere and we'll say the 
    // ray doesn't intersect
    if (t0 < 0 && t1 > 0 ) return { intersects: false }
    // If t1 is less than 0, the source is past the sphere and therefore the ray doesn't intersect
    if (t1 < 0) return { intersects: false }

    // If none of the above checks pass, the ray intersects with the sphere
    return { intersects: true, distance: t0 }
}