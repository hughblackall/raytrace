function Material(colour, albedo, specularExponent) {
    this.diffuseColour = colour
    this.albedo = albedo
    this.specularExponent = specularExponent
}