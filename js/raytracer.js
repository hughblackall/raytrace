function init() {
	console.log('init() called')
	const renderTimeId = 'Render time'
	
	var canvas = document.getElementById('screen-sink')
	if (canvas.getContext) {
		var ctx = canvas.getContext('2d')

		console.time(renderTimeId)
		render(ctx)
		console.timeEnd(renderTimeId)
	}
}

function render(ctx) {
	console.log('render() called')
	
	const width  = 1024  // Framebuffer width in pixels
	const height = 576   // Framebuffer height in pixels
	let fov = 1.3        // Field of view in radians

	let source = [0, 0, 0]  // Location of the ray source (the "camera")

	let ivory     = new Material([102, 102, 77], {diffuse: 0.6, specular:  0.3, reflective: 0.1},   50)
	let redRubber = new Material([77,  26,  26], {diffuse: 0.9, specular:  0.1, reflective: 0.0},   10)
	let mirror    = new Material([50,  50,  50], {diffuse: 0.4, specular: 10.0, reflective: 0.7}, 1425)

	let spheres = [
		new Sphere([-3,    0,   -16], 2, ivory),
		new Sphere([-4,   -2.5, -12], 2, redRubber),
		new Sphere([ 1.5, -0.5, -18], 5, redRubber),
		new Sphere([ 7,    5,   -18], 4, mirror),
		new Sphere([-10,   6,   -18], 6, mirror)
	]

	let lights = [
		new Light([ -20, 20,  20], 1.5),
		new Light([  30, 50, -25], 1.8),
		new Light([  30, 20,  20], 1.7)
	] 

	let framebuffer = ctx.createImageData(width, height)

	for (let row=0; row < height; row++) {
		for (let col=0; col < width; col++) {
			let redIndex   = row * width * 4 + col * 4
			let greenIndex = redIndex + 1
			let blueIndex  = redIndex + 2
			let alphaIndex = redIndex + 3

			let direction = pixelDirection(col, row, width, height, fov)
			let pixel = castRay(source, direction, spheres, lights)
			
			framebuffer.data[redIndex]   = pixel[0]
			framebuffer.data[greenIndex] = pixel[1]
			framebuffer.data[blueIndex]  = pixel[2]
			framebuffer.data[alphaIndex] = 255
		}
	}

	ctx.putImageData(framebuffer, 0, 0)
}

/**
 * Cast a single ray in the given direction and return the colour that the ray 'sees'
 */
function castRay(source, direction, spheres, lights, depth = 0) {
	
	let rayIntersect = sceneIntersect(source, direction, spheres)
	
	if (rayIntersect.intersects && depth <= 4) {
		// If the ray intersects with a sphere, calculate the colour that should be returned based
		// on the direction and intensity of light sources and the material of the sphere
		
		// First, loop through each light source to find its contribution to diffuse and specular
		// lighting at this intersection
		let diffuseLightIntensity = 0, specularLightIntensity = 0
		for (let i=0; i < lights.length; i++) {
			
			// Get the direction and distance of this light from the point where the ray intersects 
			// the sphere
			let lightDirection = vectorNormalise( vectorSubtract( lights[i].position, rayIntersect.hit) )
			let lightDistance  = vectorMagnitude( vectorSubtract( lights[i].position, rayIntersect.hit) )
			
			// Check if the intersection lies in a shadow of another sphere for this light
			// First move the intersect location slightly in the normal direction to ensure the ray
			// used to check for objects in front of the light doesn't intersect with the sphere itself
			let shadowSource = vectorDotProduct(lightDirection, rayIntersect.normal) < 0
								   ? vectorSubtract( rayIntersect.hit, vectorScale(rayIntersect.normal, 1e-3) )
								   : vectorAdd(      rayIntersect.hit, vectorScale(rayIntersect.normal, 1e-3) )
			let shadowIntersect = sceneIntersect(shadowSource, lightDirection, spheres)
			let lightBehindIntersect
			if (shadowIntersect.intersects) 
				lightBehindIntersect = vectorMagnitude( vectorSubtract(shadowIntersect.hit, shadowSource) ) < lightDistance
			// If there is nothing between the light and the sphere or the light is in front of the 
			// sphere, continue adding the impact of the light to the resulting colour. Otherwise 
			// skip adding the impact of this light.
			if (!shadowIntersect.intersects || !lightBehindIntersect) {
				
				// Add the diffuse light intensity for this light by looking at the light intensity 
				// and (dot products are great for this) the angle of incidence of the light on the 
				// sphere
				let angleOfIncidenceFactor = vectorDotProduct(lightDirection, rayIntersect.normal)
				if (angleOfIncidenceFactor < 0) angleOfIncidenceFactor = 0
				diffuseLightIntensity += lights[i].intensity * angleOfIncidenceFactor
				
				// Add the specular light intensity for this light by finding the reflection direction
				// and comparing it to the ray direction, taking into account the specular exponent of
				// the sphere material
				let reflection = reflect( vectorNegate(lightDirection), rayIntersect.normal )
				let projectionReflectionOnRay = -vectorDotProduct(reflection, direction)
				if (projectionReflectionOnRay < 0) projectionReflectionOnRay = 0
				let specularFactor = Math.pow(projectionReflectionOnRay, rayIntersect.material.specularExponent)
				specularLightIntensity += lights[i].intensity * specularFactor
			}
		}

		// Recursively cast a new ray from the intersection of this ray in the direction of
		// reflection to get the colour contribution of reflections at this intersection
		let reflectionDirection = vectorNormalise( reflect(direction, rayIntersect.normal) )
		// Move the intersect location in the normal direction again, so the new ray doesn't 
		// intersect with the reflecting sphere
		let reflectionSource = vectorDotProduct(reflectionDirection, rayIntersect.normal) < 0
		                         ? vectorSubtract( rayIntersect.hit, vectorScale(rayIntersect.normal, 1e-3) )
								 : vectorAdd(      rayIntersect.hit, vectorScale(rayIntersect.normal, 1e-3) )
		let reflectionColour = castRay(reflectionSource, reflectionDirection, spheres, lights, depth + 1)

		// Put together the contributions of diffuse and specular lighting i.e. multiply material 
		// colour by light intensity and material albedo
		let diffuseTerm = vectorScale( vectorScale(rayIntersect.material.diffuseColour, diffuseLightIntensity) , rayIntersect.material.albedo.diffuse )
		let specularTerm = vectorScale( vectorScale([255, 255, 255], specularLightIntensity), rayIntersect.material.albedo.specular )
		let reflectionTerm = vectorScale(reflectionColour, rayIntersect.material.albedo.reflective )
		return vectorAdd( vectorAdd(diffuseTerm, specularTerm), reflectionTerm )

	} else {
		// If there's no intersection, return the background colour
		return [51, 179, 204]
	}
}

/**
 * Get the x and y components of the direction of a pixel from the source
 */
function pixelDirection(col, row, width, height, fov) {

	// Let center of the screen be in the -z direction from the source
	// The x component is multiplied by the screen aspect ratio. This is necessary as we're using
	// the same fov for both the horizontal and vertical dimensions, otherwise our screen would be 
	// square in world space.
	// The -1s shift the screen to center around the z axis
	// Because our framebuffer rows are indexed with smaller numbers at the top of the screen, 
	// which is at odds with how spatial coordinate systems are often defined (positive y is 
	// usually up), we need to flip the image by applying a - to our y component 
	let x =  (2 * (col + 0.5) / width  - 1) * Math.tan(fov/2) * width/height
	let y = -(2 * (row + 0.5) / height - 1) * Math.tan(fov/2)
	return vectorNormalise([x, y, -1])
}

/**
 * Check intersection of a single ray with all spheres in scene and return details about the 
 * closest intersect
 */
function sceneIntersect(source, direction, spheres) {

	let result = {intersects: false}
	let closestSphereDistance = Number.MAX_VALUE

	// Loop through each sphere
	for (let i=0; i < spheres.length; i++) {
		// Check if the ray intersects with the current sphere
		let rayIntersect = spheres[i].rayIntersect(source, direction)
		// If it does, and it's the closest intersect so far, update details to be returned
		if (rayIntersect.intersects && rayIntersect.distance < closestSphereDistance) {
			closestSphereDistance = rayIntersect.distance

			// hit is the location of the intersection
			result.hit = vectorAdd( source, vectorScale(direction, rayIntersect.distance) )
			// normal gives the direction perpendicular to the sphere's surface at the location of
			// the hit
			result.normal = vectorNormalise( vectorSubtract(result.hit, spheres[i].position) )
			result.material = spheres[i].material
			result.intersects = true
		}
	}

	return result
}

/**
 * Find a reflection unit vector from an incident vector using
 *   I - 2(I⋅N)N
 * where I is the unit incident vector (pointing towards the reflecting surface) and N is the unit 
 * normal vector
 */
function reflect(incident, normal) {
	// Find the projection of the incident vector on the normal. We scale this by 2 in the next 
	// step to make the base of an isosceles triangle, with the reflection vector at the 'pointy
	// end' and the normal along the base
	let projectionIncidentOnNormal = vectorScale( normal, vectorDotProduct(incident, normal) )
	// The incident vector points towards the reflecting surface and so the above dot product will
	// be negative, therefore we need to subtract it from the incident to give the reflection vector
	return vectorSubtract( incident, vectorScale(projectionIncidentOnNormal, 2) )
}