/**
 * Add two three dimensional vectors
 */
function vectorAdd(vector1, vector2) {
    let vector3 = [
        vector1[0] + vector2[0],
        vector1[1] + vector2[1],
        vector1[2] + vector2[2]
    ]
    return vector3
}

/**
 * Subtract two three dimensional vectors
 */
function vectorSubtract(vector1, vector2) {
    let vector3 = [
        vector1[0] - vector2[0],
        vector1[1] - vector2[1],
        vector1[2] - vector2[2]
    ]
    return vector3
}

/**
 * Find the dot product of two three dimensional vectors
 */
function vectorDotProduct(vector1, vector2) {
    return vector1[0] * vector2[0]
         + vector1[1] * vector2[1]
         + vector1[2] * vector2[2]
}

/**
 * Scale a three dimensional vector
 */
function vectorScale(vector, scalar) {
    return [vector[0]*scalar, vector[1]*scalar, vector[2]*scalar]
}

/**
 * Find the magnitude of a three dimensional vector
 */
function vectorMagnitude(vector) {
    return Math.sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2])
}

/**
 * Scale a three dimensional vector so that its magnitude is 1, also known as normalisation
 */
function vectorNormalise(vector) {
    let magnitude = vectorMagnitude(vector)
    if  (magnitude > 0) return vectorScale(vector, 1/magnitude)
    else return vector
}

/**
 * Find the negative of a three dimensional vector
 */
function vectorNegate(vector) {
    return vectorSubtract([0, 0, 0], vector)
}